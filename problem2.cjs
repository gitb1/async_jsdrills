const fs = require("fs");


// Using callbacks and the fs module's asynchronous functions, do the following:
// 1. Read the given file lipsum.txt
// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
// 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
// 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
// */

function optOnFiles() {
    // 1. Read the given file lipsum.txt

  fs.readFile("./lipsum_1.txt", "utf-8", (err, data) => {
    if (err) {
      console.log("Error found", err.message);
    } else {
      console.log("Reading lipsum");
      // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

      const uppercaseData = data.toUpperCase();
      console.log(' CHanged to Uppercase');
      const upperCasefile = "./upper_case.txt";

      fs.writeFile(upperCasefile, uppercaseData, (err) => {
        if (err) {
          console.log("Error found", err.message);
        } else {
            console.log('  Uppercase file created');
          fs.writeFile("./filenames.txt", upperCasefile, (err) => {
            if (err) {
              console.log("Error found", err.message);
            } else {
                console.log(' Written filename in filenames.txt');
    // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

              fs.readFile(upperCasefile, "utf-8", (err, data) => {
                if (err) {
                  console.log("Error found", err.message);
                } else {
                  const lowercaseContent = data.toLowerCase();
                  const splitteddata = lowercaseContent.split('.');
                    console.log(' Data is changed into lowercase and spiltted into sentences'); 
                  const sentences = "./sentences.txt";
                  fs.writeFile(sentences, splitteddata.join("\n"), (err) => {
                    if (err) {
                      console.log("Error found", err.message);
                    } else {
                      fs.appendFile(
                        "./filenames.txt",
                        "\n" + sentences,
                        (err) => {
                          if (err) {
                            console.log("Error found", err.message);
                          }
    // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                            console.log(' Written filename in filenames.txt');

                          fs.readFile(
                            "./sentences.txt",
                            "utf-8",
                            (err, data) => {
                              if (err) {
                                console.log("Error found", err.message);
                              } else {

                                const restData = data.split("\n").sort();
                                const newFilePath = "./newFilePath.txt";
                                console.log(' Sentences are sorted');

                                fs.writeFile(
                                  newFilePath,
                                  restData.join("\n"),
                                  (err) => {
                                    if (err) {
                                      console.log("Error found", err.message);
                                    }
                                    console.log(' Sorted data written in sentence file');

                                    fs.appendFile(
                                      "./filenames.txt",
                                      "\n" + newFilePath,
                                      (err) => {
                                        if (err) {
                                          console.log(
                                            "Error found",
                                            err.message
                                          );
                                        }
// 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                                         console.log(' Written filename in filenames.txt');

                                        fs.readFile(
                                          "./filenames.txt",'utf-8',
                                          (err, data)=> {
                                            if (err) {
                                              console.error(
                                                "Error reading filenames.txt:",
                                                err
                                              );
                                              return;
                                            }
                                            console.log(' Reading filenames.txt');

                                            //data=data.toString();
                                           const filenames=data.split('\n');
                                            filenames.forEach(filename => {
                                                try {
                                                   

                                                        fs.unlink(filename,(err)=>{
                                                            if (err) {
                                                                console.error(
                                                                  "Error reading filenames.txt:",
                                                                  err
                                                                )
                                                            }
                                                       });
                                                        console.log(`${filename} deleted.`);
                                                    
                                                } catch (err) {
                                                    console.error(`Error deleting ${filename}:`, err);
                                                }
                                            });
                                             
                                           
                                              console.log(
                                                "All tasks completed successfully!"
                                              );
                                            
                                          }
                                          
                                        );
                                      }
                                    );
                                  }
                                );
                              }
                            }
                          );
                        }
                      );
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports=optOnFiles;