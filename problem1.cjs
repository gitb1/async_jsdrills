const fs = require('fs');
const path = require('path');

function rec(directoryPath, count, val) {
    if (val < count) {
        const dPath = path.join(directoryPath, `file${val}.json`);
        fs.writeFile(dPath, 'data', (err) => {
            if (err) {
                console.error(err.message);
            }
            console.log(`file${val} created`);

            fs.unlink(dPath, (err) => {
                if (err) {
                    console.error(err.message);
                }
                console.log(`file${val} deleted`);
                rec(directoryPath, count, val + 1); // Recursive call
            });
        });
    } else {
        return;
    }
}

function createRandomjsonFiles(count, directoryPath, callback) {
    fs.mkdir(directoryPath, (error) => {
        if (error) {
            console.error(error.message);
            return;
        }
        console.log(`Directory created.`);
        rec(directoryPath, count, 0); // Call recursive function
        if (typeof callback === 'function') {
            callback(); // Call the callback function if provided
        }
    });
}

module.exports=createRandomjsonFiles;








